import 'package:flutter/cupertino.dart';
import 'package:ileri_donusum/classes/address_model.dart';
import 'package:ileri_donusum/classes/request.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User extends ChangeNotifier {
  int _id;
  String _firstName;
  String _lastName;
  String _gender;
  String _birthDay;
  String _phoneNumber;
  String _eMail;
  String _password;
  String _verifyEmail;
  int _recordTime;
  bool _isBlock;
  List<AddressModel> addressList;
  List<Request> requestList;
  SharedPreferences shrdPrf;
  User([
    this._id,
    this._firstName,
    this._lastName,
    this._gender,
    this._birthDay,
    this._phoneNumber,
    this._eMail,
    this._password,
    this._verifyEmail,
    this._recordTime,
    this._isBlock,
    this.addressList,
  ]) {
    addressList = List<AddressModel>();
    requestList = List<Request>();

    notifyListeners();
  }

  factory User.fromJson(Map<dynamic, dynamic> json) {
    return User(
      json["id"],
      json["firstName"],
      json["lastName"],
      json["gender"],
      json["birthDay"],
      json["phoneNumber"],
      json["email"],
      json["password"],
      json["verifyEmail"],
      json["recordTime"],
      json["isBlock"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": _id,
      "firstName": _firstName,
      "lastName": _lastName,
      "gender": _gender,
      "birthDaty": _birthDay,
      "phoneNumber": _phoneNumber,
      "email": _eMail,
      "password": _password,
      "verifyEmail": _verifyEmail,
      "recordTime": _recordTime,
      "isBlock": _isBlock,
    };
  }

  void changeValues(User user) {
    this._firstName = user._firstName;
    this._lastName = user._lastName;
    this._gender = user._gender;
    this._phoneNumber = user._phoneNumber;
    this._eMail = user._eMail;
    this._password = user._password;
    this._id = user._id;
    this._birthDay = user._birthDay;
    this._verifyEmail = user._verifyEmail;
    this._recordTime = user._recordTime;
    this._isBlock = user._isBlock;
  }

  Future<void> createSharedPref() async {
    shrdPrf = await SharedPreferences.getInstance();
  }

  Future<void> saveUserInfoToShrdPref(String userInfo) async {
    await createSharedPref();
    shrdPrf.setString("userInfo", userInfo);
  }

  void addToList(dynamic item) {
    item.runtimeType.toString() == "AddressModel"
        ? addressList.add(item)
        : requestList.add(item);

    //notifyListeners();
  }

  void changeAddressItems(int index, AddressModel item) {
    addressList[index] = item;
    //notifyListeners();
  }

  void removeFromList(dynamic item) {
    item.runtimeType.toString() == "AddressModel"
        ? addressList.remove(item)
        : requestList.remove(item);
    //notifyListeners();
  }

  int get getId {
    return _id;
  }

  String get getEmail {
    return _eMail;
  }

  String get getPassword {
    return _password;
  }

  String get getFirstName {
    return _firstName;
  }

  String get getLastName {
    return _lastName;
  }

  String get getGender {
    return _gender;
  }

  String get getPhoneNumber {
    return _phoneNumber;
  }

  void setEmail(String value) {
    _eMail = value;
  }

  void setPassword(String value) {
    _password = value;
  }
}
